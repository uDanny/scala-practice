# Scala exercise
Some exercises resolved in pure functional language (Scala)
1. Given the list of arbitrary atoms L. Applying the recursion and the collector variables to construct the list of atoms that appear in L only once.

2. Should be created a function (DUPLICATE-ELEMENTS L), which verifies the repeated appearence of an element (atom or list) in L.
Example of expected outputs:
(DUPLICATE-ELEMENTS ‘(a b a c)) -> T
(DUPLICATE-ELEMENTS ‘(a b (a b)) -> NIL
(DUPLICATE-ELEMENTS ‘(a b (a b) c d (a b))) -> T

3. Non-determinestic Finite Automata (NFA) with epsilon move to Determinestic Finite Automata (DFA) Conversion