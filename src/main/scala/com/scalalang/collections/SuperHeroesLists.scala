package com.scalalang.collections

import scala.collection.mutable.ListBuffer

object SuperHeroesLists {
  def main(args: Array[String]): Unit = {
    val imutableHeroes = List("Wonder Woman", "Thor", "Superman", "Batman")
    val numbers = 1 :: 2 :: 3 :: Nil //define a list of constants

    println(imutableHeroes.head) //return first element
    println(imutableHeroes.tail) //return everything except the first element

    val readableHeroes = imutableHeroes.mkString(" | ")

    val pretifyHeroes = imutableHeroes.mkString("[ ", ", ", " ]")

    println(readableHeroes)
    println(pretifyHeroes)

    val mutableHeroes = ListBuffer("Wonder Woman", "Thor", "Superman", "Batman")
    mutableHeroes += "Raven"
    "Captain America" +=: mutableHeroes //same as upper, reverse order manner
    mutableHeroes.foreach(println)

    val captainAmerica = mutableHeroes(0)
    mutableHeroes(2) = "Iron Man"
    mutableHeroes -= "Batman"

    mutableHeroes.foreach(println)
  }
}
