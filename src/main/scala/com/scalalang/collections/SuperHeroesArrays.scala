package com.scalalang.collections

import scala.collection.mutable.ArrayBuffer

object SuperHeroesArrays {
  def main(args: Array[String]): Unit = {
    val numbers = new Array[Int](10)
    val words = new Array[String](10)
    val heroes = Array("Wonder Woman", "Thor", "Superman", "Batman")
    println(heroes(3))

    val mutableHeroes = ArrayBuffer("Wonder Woman(1)", "Thor(1)", "Superman(1)", "Batman(1)")
    mutableHeroes += "Iron Man"
    mutableHeroes ++= heroes

    println(mutableHeroes)

    mutableHeroes.trimEnd(2)
    mutableHeroes.update(0, "Flash")

    println(mutableHeroes)

    for (hero <- mutableHeroes) println("iterate " + hero)
    for (index <- 0 until mutableHeroes.length) println(mutableHeroes(index) + " iteration: " + index)
    for (index <- mutableHeroes.indices) println(mutableHeroes(index) + " iteration: " + index)

      val spaceHeroes = for (hero <- mutableHeroes if hero.contains(" ")) yield hero
    spaceHeroes.foreach(println)
  }
}
