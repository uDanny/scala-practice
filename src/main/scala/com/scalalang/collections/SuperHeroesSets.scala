package com.scalalang.collections

import scala.collection.mutable

object SuperHeroesSets {
  def main(args: Array[String]): Unit = {
    //from default package is imutable, the following are mutable
    val numbers = mutable.Set(1, 2, 3, 4, 5) //no duplicates, does not respect the order (like HashSet)
    numbers.foreach(println)

    val orderedSet = mutable.LinkedHashSet(1, 2, 3, 4, 5)
    orderedSet += -5
    orderedSet -= 2
    orderedSet.foreach(println)

    val unionResult = numbers.union(orderedSet)
    val intersectResult = numbers.intersect(orderedSet)

    println(unionResult)
    println(intersectResult)


  }
}
