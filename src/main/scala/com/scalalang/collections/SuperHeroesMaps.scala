package com.scalalang.collections

import scala.collection.mutable

object SuperHeroesMaps {
  def main(args: Array[String]): Unit = {
    //Map from default package is immutable
    val avengers = Map("Iron Man" -> 12,
      "Black Window" -> 18,
      "Captain America" -> 8,
      "Scarlet Witch" -> 10)

    val scarletWithc = avengers("Scarlet Witch") //get value by key

    val ironMan = avengers.getOrElse("iron Man", 0) // if not found return 0

    println(avengers.keys)
    println(avengers.values)

    for ((key, value) <- avengers) println(key + " : " + value)

    for (value <- avengers) println(value)


    //mutable map from collections.mutable package
    val mutableAvengers = mutable.Map(
      "Iron Man" -> 12,
      "Black Window" -> 18,
      "Captain America" -> 8,
      "Scarlet Witch" -> 10)

    mutableAvengers("Iron Man") = 9

    mutableAvengers += ("Hawkeye" -> 20)
    mutableAvengers -= "Captain America"

    println("--")
    mutableAvengers.foreach(println)

    //tuples - less 22 objects, not typified
    val haweye = ("Clint", "Waverly", "Iowa", 'M') // literal tuple
    val hawkName = haweye._1 //first index
    val hawkBirthPlace = haweye._2

    haweye.productIterator.foreach(println)
  }
}
