package com.scalalang.introduction

object SuperHeroes {
  def main(args: Array[String]): Unit = {
    println("Hello World!!!")

    val superMan = new SuperHero("Superman", "Clark Kent", "Krypton", 'M')

    val batman = new SuperHero("Batman")

    val spiderMan = new SuperHero("Spider-Man", "New York")

    val hulk = new SuperHero("Hulk", "Robert Bruce Banner", "Dayton Ohio", 'M')

    hulk.age = 20;

    case class PowerStats(strength: Int, speed: Int)

    val wonderWoman = PowerStats(97, 78)

    val thor = PowerStats(100, 80)
    wonderWoman.speed

    def findHero(power: PowerStats) = power match {
      case PowerStats(100,80) => "thor"
      case PowerStats(97,78) => "wonder woman"
      case PowerStats(_,_) => "Match was not found"
    }

    val hero = findHero(thor)

    println(hero)

    var loky = new Villain("Loki", "Asgard")
    loky.details()
    loky.age
  }
}
