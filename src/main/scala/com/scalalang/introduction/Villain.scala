package com.scalalang.introduction

class Villain(name:String, birthPlace:String) extends SuperHero (name: String, birthPlace: String){

  println("A Villain was created")

  override def details(): Unit = {
    super.details()
    println(" is a notorius Villain.")
  }
}
