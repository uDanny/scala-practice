package com.scalalang.introduction

class SuperHero(val name: String, val nickname: String, val birthPlace: String, val gender: Char) {
  println("A hero was created : " + name)

  def this(name: String) {
    this(name, "", "", ' ')
  }

  def this(name: String, birthPlace: String) {
    this(name, "", birthPlace, ' ')
  }

  def details() = {
    println("name : " + name)
    println("nickname : " + nickname)
    println("birthPlace : " + birthPlace)
    println("gender : " + gender)
  }

  private var heroAge = 0

  def age = heroAge

  def age_=(newAge: Int) = {
    if (newAge > heroAge)
      heroAge = newAge
    else
      heroAge
  }

}
