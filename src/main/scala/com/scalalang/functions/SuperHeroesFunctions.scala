package com.scalalang.functions

object SuperHeroesFunctions {
  def main(args: Array[String]): Unit = {
    val heroes = Array("Wonder Woman", "Thor", "Superman", "Batman")

    val lowerC = (value: String) => value.toLowerCase()

    for (elem <- heroes) {
      println(lowerC(elem))
    }

    //anonymus functions
//    def updateRankings(x: Int) = x - 3
//    //or
//    (x: Int) =>
//      x - 3

      val avengers = Map("Iron Man" -> 12,
        "Black Window" -> 18,
        "Captain America" -> 8,
        "Scarlet Witch" -> 10)
      //example using anonymus functions( as they was defined and called )
      val newRankings = avengers.values.map((x: Int) => x - 3)
      newRankings.foreach(println)

  }
}
