package com.scalalang.functions

object SuperHeroesAppliedFunctions {
  def main(args: Array[String]): Unit = {
    val heroes = Array("Wonder Woman", "Thor", "Superman", "Batman")

    val lowerC = (value: String) => value.toLowerCase()

    val avengers = Map("Iron Man" -> 12,
      "Black Window" -> 18,
      "Captain America" -> 8,
      "Scarlet Witch" -> 10)
    val newRankings = avengers.values.map((x: Int) => x - 3)


    def multiply(x: Int, y: Int, z: Int) = x * y * z

    val result = multiply(1, 2, 3)
    println(result)

    val someResult = multiply(1, 2, _: Int) // we made other function with hardcoded first 2 values
    println(someResult(3)) // pass 3rd argument with value 3

    val noneResult = multiply _ //just pass all arguments to multiply function
    println(noneResult(1, 2, 3))

    //currying

    def add(x: Int) = (y: Int) => x + y // everything after =, is a function, and add function just return another function
    val sum = add(8) // it returns a function, and send x parameter
    println(sum(2))

    def addition(x: Int)(y: Int) = x + y

    println(add(8)(4))

    //high order functions

    heroes.foreach(println) // println is a function that is passed as a parameter
    val lHeroes = heroes.map(data => data.toLowerCase()) //.map is executing a function on each element
    val lHeroes1 = heroes.map(_.toLowerCase()) // _ is returning each element data

    lHeroes1.foreach(println)

    val man = heroes.filter(_.contains("man")) // function return element only if the filter condition is true
    man.foreach(println)

    val upperMan = heroes.filter(_.contains("man")).map(_.toUpperCase())
    man.foreach(println)

    //numbers Array functions
    val numbers = Array(1, 2, 3, 4, 5, 4, 4, 3, 5)
    val less = numbers.dropWhile(_ < 3)
    less.foreach(println)

    val howMany = numbers.count(_ <= 4)
    println(howMany)

    val isItInThere = numbers.indexWhere(_ == 3)
    println(isItInThere)

    val ironManR = avengers.getOrElse("Iron Man", 0)
    val blackWindowR = avengers.getOrElse("Black Window", 0)
    val captainAmericaR = avengers.getOrElse("Captain America", 0)

    //3 int parameters and 4th is a fucntion that has 3 parameters and return int,
    // inside the compare function, is called the function from parameter
    def compare(aveenge1: Int, aveenge2: Int, aveenge3: Int,
                f: (Int, Int, Int) => Int) = f(aveenge1, aveenge2, aveenge3)

    //_ = wildards that represent avengers, and min is the min function between each 2 elements
    val bestRanked = compare(ironManR, blackWindowR, captainAmericaR, _ min _ min _)
    println(bestRanked)

    val worstRanked = compare(ironManR, blackWindowR, captainAmericaR, _ max _ max _)
    println(worstRanked)
  }
}
