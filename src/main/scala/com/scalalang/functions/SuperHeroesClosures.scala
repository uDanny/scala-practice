package com.scalalang.functions

object SuperHeroesClosures {
  def main(args: Array[String]): Unit = {
    var prefix = "Lightning "

    def reverseIt(name: String) = (name + prefix).reverse

    val thor = reverseIt("Thor")

    prefix = "Super Fast "

    val wonderWomen = reverseIt("Wonder Woman")

    println(thor)
    println(wonderWomen)

  }
}
