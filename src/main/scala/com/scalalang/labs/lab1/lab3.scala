package com.scalalang.labs

package object lab3 {

  object EnumStates extends Enumeration {
    val q0, q1, q2 = Value
  }

  object EnumTransition extends Enumeration {
    val a, b, c, e = Value
  }

  case class StatesObj(actualState: EnumStates.Value,
                       transition: Map[EnumTransition.Value, List[EnumStates.Value]],
                       isFinal: Boolean)


  case class FinalStatesObj(actualState: List[EnumStates.Value],
                            transition: EnumTransition.Value,
                            destination: List[EnumStates.Value],
                            isFinal: Boolean)

  def main(args: Array[String]): Unit = {

    val q0 = StatesObj(EnumStates.q0,
      Map(
        EnumTransition.a -> List[EnumStates.Value](EnumStates.q0),
        EnumTransition.e -> List[EnumStates.Value](EnumStates.q1)),
      isFinal = false)
    val q1 = StatesObj(EnumStates.q1,
      Map(
        EnumTransition.b -> List[EnumStates.Value](EnumStates.q1),
        EnumTransition.e -> List[EnumStates.Value](EnumStates.q2)),
      isFinal = false)
    val q2 = StatesObj(EnumStates.q2,
      Map(
        EnumTransition.c -> List[EnumStates.Value](EnumStates.q2)),
      isFinal = true)
    val states = List[StatesObj](q0, q1, q2)

    val eTransitionsMap = states.flatMap(
      x => Map(x.actualState -> x.transition.filter(
        _._1.equals(EnumTransition.e)).values.flatten.toList
      )).filter(_._2.nonEmpty).toMap

    eTransitionsMap.foreach(x => println(x))

    System.out.println("___")

    def someMethod(eachElement: (EnumStates.Value, List[EnumStates.Value])): List[EnumStates.Value] = {

      eachElement._2
        .map(x => {
          if (x != eachElement._1 && eTransitionsMap.contains(x)) {
            return eachElement._1 :: x :: someMethod(x, eTransitionsMap(x))
          }
          return List(x)
        }).distinct
    }

    val values = eTransitionsMap.map(someMethod).toList
    println(values)

    def getSubLists(list: List[EnumStates.Value]): List[List[EnumStates.Value]] = {
      if (list.size > 1) {
        list :: getSubLists(list.slice(1, list.size))
      } else {
        List(list)
      }
    }

    val allValues = values.toStream.flatMap(x => getSubLists(x)).distinct.toList

    println(allValues)

    def getAllStates(list: List[EnumStates.Value]): List[EnumTransition.Value] = {
      list.flatMap(
        eachStateValue =>
          states.find(y => y.actualState == eachStateValue).get
            .transition.keys.filter(_ != EnumTransition.e).toList)
    }

    def getDestination(transition: EnumTransition.Value, source: List[EnumStates.Value]): List[EnumStates.Value] = {
      states.filter(x => source.contains(x.actualState)).flatMap(
        x => x.transition.filter(_._1 == transition).values).flatten
    }

    def checkIsFinal(source: List[EnumStates.Value]): Boolean = {
      source.exists(x => states.find(_.actualState == x).get.isFinal)
    }

    def makeFinalObj(transition: EnumTransition.Value, source: List[EnumStates.Value], destinations: List[EnumStates.Value]): FinalStatesObj = {
      val finalDestination = destinations.map(x => allValues.find(y => y.head == x))
      if (finalDestination.nonEmpty)
        FinalStatesObj(source, transition, finalDestination.flatten.flatten, isFinal = checkIsFinal(source))
      else
        null
    }

    val finalResult = allValues.map(eachFinalNode => {
      val eachTransition = getAllStates(eachFinalNode)
      eachTransition.map(x => {
        val destinations = getDestination(x, eachFinalNode)
        makeFinalObj(x, eachFinalNode, destinations)
      })
    }).filter(x => x != null)
    println(finalResult)


    println("\n Detailed println:")

    finalResult.foreach(x => {
      println("\n" + x.head.actualState.toString().replaceAll("List", "") + " - isFinal: " + x.head.isFinal)
      x.foreach(y => println("\t" +
        y.actualState.toString().replaceAll("List", "")
        + "-" + y.transition + "->"
        + y.destination.toString().replaceAll("List", "")))
    })
  }

}
