//l1
val arbitraryList = Array("a", "b", "c", "a", "g", "c")

def checkHasDuplicates(searchElement: String, list: Array[String]): Array[String] =
  for (eachElem <- list if eachElem.equals(searchElement)) yield searchElement

val uniqueValues = for (eachElement <- arbitraryList if checkHasDuplicates(eachElement, arbitraryList).length == 1) yield eachElement

uniqueValues.foreach(println)