package com.scalalang.labs

package object lab2 {
  def main(args: Array[String]): Unit = {
    //    val arbitraryList = List[Object]("a", "b", "a", "b")
    //    val arbitraryList = List[Object]("a", List("b"), "c", "a", "g", "c")
    //    val arbitraryList = List[Object]("a", "b", List("a", "b"))
    val arbitraryList = List[Object](List(List("d")),"a", "b", List("a", "c"), List(List("a", "d")))

    println("initial list:")
    println(arbitraryList)

    def getEachLayer(list: List[Object]): List[List[String]] = {
      val thisLayer = list.collect { case x: String => x }
      val nextLayer: List[Object] = list.collect { case x: List[Object] => x }.flatten
      if (nextLayer.nonEmpty) {
        return thisLayer :: getEachLayer(nextLayer)
      }
      List(thisLayer)
    }

    val layers = getEachLayer(arbitraryList)
    println("Layers: ")
    layers.foreach(x => println(" " + x))

    def hasDuplicates(list: List[String]): Boolean = list.distinct.size != list.size

    val message = if (layers.exists(hasDuplicates)) "has" else "doesn't have"
    print("list " + message + " duplicates on the same layer")
  }
}
